import { useState, useEffect,useContext } from 'react';
import { Form, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams} from 'react-router-dom'

import Swal from 'sweetalert2';

export default function UserDetails() {

  const {userId} = useParams();
  
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password, setPassword] = useState("");

  function UpdateUser(e){
  
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/updateUser`,{
                method: "PUT",
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: `Bearer ${localStorage.getItem('token')}`

                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    mobileNo: mobileNo,
                    password: password
                })
        })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if (data === true) {

                Swal.fire({
                    title: "User Information Updated!",
                    icon: "success",
                    text: "Kindly provide another email to complete registration."
                })

            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please, try again."
                })
            }
    })
  }


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/userDetails`)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        setFirstName(data.firstName);
        setLastName(data.lastName);
        setEmail(data.email);
        setMobileNo(data.mobileNo);
        setPassword(data.password);
      })
  }, [userId])

  return (
<Col>
    <Form className="mb-3 mt-5">
      <Row>
        <Col>
          <Form.Label>First Name:</Form.Label>
          <Form.Control 
            type="firstName" 
            value={firstName} 
            onChange={(e) => {setFirstName(e.target.value)}}
            placeholder="First Name"
            required 
           />
        </Col>
        <Col>
          <Form.Label>Last Name:</Form.Label>
           <Form.Control 
             type="name" 
             value={lastName} 
             onChange={(e) => {setLastName(e.target.value)}}
             placeholder="Last Name"
             required 
            />
        </Col>
      </Row>
      <Row>
        <Col className="col-4">
          <Form.Label>Mobile Number:</Form.Label>
          <Form.Control 
            type="text"
                value={mobileNo}
                onChange={(e) => {setMobileNo(e.target.value)}}
                placeholder="0999999999"
            required 
           />
        </Col>
        <Col className="col-8">
          <Form.Label>Password:</Form.Label>
           <Form.Control 
             type="name" 
             value={""} 
             onChange={(e) => {setPassword(e.target.value)}}
             placeholder="*******"
             required 
            />
        </Col>
      </Row>
    </Form>

    <Button className="me-2" variant="success" onClick={() => UpdateUser()}>Update Information</Button>

    <Card className="text-center mt-5 mb-5">
      <Card.Header>Abandoned Cart: createdOn</Card.Header>
      <Card.Body>
        <Card.Title>ORDER INFORMATION</Card.Title>
        <Card.Text>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget nibh et tortor luctus cursus. Donec viverra lorem sit amet elementum auctor. Fusce volutpat libero vitae mi mollis posuere. Vestibulum vulputate elit ac dui bibendum iaculis. Pellentesque quis elit tellus. Proin consectetur purus vitae sapien sodales, vel lobortis odio tristique. Curabitur vitae velit est. Etiam facilisis magna orci, a gravida justo fringilla sit amet. Sed auctor auctor magna sit amet consequat. Nullam fringilla accumsan dignissim. Mauris at massa odio.

          Curabitur augue arcu, lobortis sit amet mauris ac, luctus lacinia sem. Cras facilisis urna nisi, ac pharetra nisi ultricies et. Phasellus blandit, felis sit amet pellentesque dignissim, diam nisl rhoncus sapien, non tempus urna nibh sit amet metus. Suspendisse quam lorem, pellentesque nec congue sed, tempus et nulla. Praesent turpis dolor, iaculis ut erat sed, vehicula dignissim arcu. Maecenas quis velit id nunc sodales vehicula sit amet ut odio. Quisque bibendum risus odio, in vehicula risus ornare in. Ut in leo fringilla, bibendum risus at, sollicitudin odio. In hac habitasse platea dictumst. Praesent porta luctus ante quis consequat. Vivamus ut lacinia dolor. Ut posuere erat vitae nunc vehicula, non eleifend risus vulputate. Duis egestas gravida libero, a fringilla neque laoreet a.
        </Card.Text>
      </Card.Body>
      <Card.Footer className="text-muted">2 days ago</Card.Footer>
    </Card>
</Col>
  );
}


