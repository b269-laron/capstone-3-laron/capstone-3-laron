import { useState, useEffect,useContext } from 'react';
import { Form, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, NavLink } from 'react-router-dom'

import Swal from 'sweetalert2';

export default function UserDetails() {

  const {user} = useContext(UserContext);


  const {userId} = useParams();
  
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password, setPassword] = useState(""); 

  function UpdateUser(e){
    
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/updateUser`,{
                method: "PUT",
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: `Bearer ${localStorage.getItem('token')}`

                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    mobileNo: mobileNo,
                    password: password
                })
        })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if (data === true) {

                Swal.fire({
                    title: "User Information Updated!",
                    icon: "success",
                    text: ""
                })

            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please, try again."
                })
            }
    })
  }

  function SetAsUser(e){
      fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setUser`,{
        method: "PATCH",
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`

        }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        if (data === true) {

                  Swal.fire({
                      title: "User Information Updated!",
                      icon: "success",
                      text: "Kindly provide another email to complete registration."
                  })

              } else {
                  Swal.fire({
                      title: "Something went wrong",
                      icon: "error",
                      text: "Please, try again."
                  })
              }
      })
    
  }

  function SetAsAdmin(e){
      fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setAdmin`,{
        method: "PATCH",
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`

        }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        if (data === true) {

                  Swal.fire({
                      title: "User Information Updated!",
                      icon: "success",
                      text: ""
                  })

              } else {
                  Swal.fire({
                      title: "Something went wrong",
                      icon: "error",
                      text: "Please, try again."
                  })
              }
      })
    
  }


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/userDetails`)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        setFirstName(data.firstName);
        setLastName(data.lastName);
        setEmail(data.email);
        setMobileNo(data.mobileNo);
        setPassword(data.password);
      })
  }, [userId])

  return (
<Col className="mb-5">
    <Form className="mb-3 mt-5">
      <Row>
        <Col>
          <Form.Label>First Name:</Form.Label>
          <Form.Control 
            type="firstName" 
            value={firstName} 
            onChange={(e) => {setFirstName(e.target.value)}}
            placeholder="First Name"
            required 
           />
        </Col>
        <Col>
          <Form.Label>Last Name:</Form.Label>
           <Form.Control 
             type="name" 
             value={lastName} 
             onChange={(e) => {setLastName(e.target.value)}}
             placeholder="Last Name"
             required 
            />
        </Col>
      </Row>
      <Row>
        <Col className="col-4">
          <Form.Label>Mobile Number:</Form.Label>
          <Form.Control 
            type="text"
                value={mobileNo}
                onChange={(e) => {setMobileNo(e.target.value)}}
                placeholder="0999999999"
            required 
           />
        </Col>
        <Col className="col-8">
          <Form.Label>Password:</Form.Label>
           <Form.Control 
             type="name" 
             value={""} 
             onChange={(e) => {setPassword(e.target.value)}}
             placeholder="*******"
             required 
            />
        </Col>
      </Row>
    </Form>

    <Button className="me-2 " variant="secondary" as={NavLink} to={"/ManageUser"}>
    Back
    </Button>
    <Button className="me-2" variant="success" onClick={() => UpdateUser()}>Update User</Button>

    {(user.isAdmin === true) ? 
    <Button className="me-2" variant="warning" onClick={() => SetAsUser()}>Set as User</Button>
    :
    <Button className="me-2" variant="danger" onClick={() => SetAsAdmin()}>Set as Admin</Button>
    }
    <Card className="text-center mt-5">
      <Card.Header>Abandoned Cart: createdOn</Card.Header>
      <Card.Body>
        <Card.Title>Product Name</Card.Title>
        <Card.Text>
          With supporting text below as a natural lead-in to additional content.
        </Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
      <Card.Footer className="text-muted">2 days ago</Card.Footer>
    </Card>
</Col>
  );
}


