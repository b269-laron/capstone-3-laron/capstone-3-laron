//import {useState, useEffect} from 'react'
import { Link } from 'react-router-dom'

import {Button, Card, Row } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function CartCard({product}) {

  const {name, description, price, stocks, _id} = product;

  const archive = () => {
  const url = `${process.env.REACT_APP_API_URL}/products/${_id}/archive`;
  console.log("Archive URL:", url);

    fetch(url, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("Archive Response:", data);
        if (data === true) {
          Swal.fire({
            title: "Successfully archived",
            icon: "success",
            text: "You have successfully archived this product.",
          });
        }
      });
  };


  const unarchive = () => {
  const url = `${process.env.REACT_APP_API_URL}/products/${_id}/unarchive`;
  console.log("Unarchive URL:", url);

    fetch(url, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("Unarchive Response:", data);
        if (data === true) {
          Swal.fire({
            title: "Successfully unarchived",
            icon: "success",
            text: "You have successfully archived this product.",
          });
        }
      });
  };

  return (

    <Card className="ManageProductsCard mt-5 p-0">

    <Row className="productsCardBody ">
      <h3 className="col-4 d-flex justify-content-center align-self-center">X</h3>

      <Card.Body className="col-8">
        <Card.Title><h6>{name}</h6></Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text className="ManageProductsCardDescription">{description}</Card.Text>
        <Card.Text >Total Price: {price}</Card.Text>
        <Card.Text >Available Stocks: {stocks}</Card.Text>
      </Card.Body>
    </Row>

      <Card.Footer>    
      <Button className="bg-success me-3" as={Link} to={`/products/${_id}`}>Update Product</Button>
      {(product.isActive === true) ?
      <Button className="bg-danger" onClick={() => archive()}>Remove Product from the list</Button>
      :
      <Button className="bg-warning" onClick={() => unarchive()}>Add Product to the list</Button>
      }   
      </Card.Footer> 
    </Card>
  );
}
