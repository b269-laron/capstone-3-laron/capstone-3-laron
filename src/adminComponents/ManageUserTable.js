import { Link } from 'react-router-dom'

import {Button, Table} from 'react-bootstrap';

export default function UserTable({user}) {

  const {firstName, lastName, email, _id} = user;

  return (
<Table bordered hover >
      <tbody>
        <tr className="d-flex justify-content-center">
          <td className="col-3">{firstName} {lastName}</td>
          <td className="col-3 ">{email}</td>
          <td className="col-3 ">{(user.isAdmin === true) ? "Admin" : "User"}</td>
          <td className="col-3 ">
          <Button className="bg-success me-auto" as={Link} to={`/users/${_id}`}>View Details</Button></td>
        </tr>
      </tbody>
</Table>

  );
}