import { useState, useEffect } from 'react';
import { Form, Button, Col } from 'react-bootstrap';

import { useParams, NavLink } from 'react-router-dom'

import Swal from 'sweetalert2';

export default function UpdateProduct() {


	// "useParams" hook that will allow us to retrieve the courseId passed via URL
	const {productId} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [image, setImage] = useState(0);


	function updateProduct(e){
	
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/updateProduct`,{
                method: "PUT",
                headers: {
                	'Content-Type': 'application/json',
                	Authorization: `Bearer ${localStorage.getItem('token')}`

                },
                body: JSON.stringify({
                    name: name,
                    description: description,
                    price: price,
                    stocks: stocks,
                    image:image
                })
        })
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {

                Swal.fire({
                    title: "Successfully Updated",
                    icon: "success",
                    text: ""
                })

            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please, try again."
                })
            }
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/productDetails`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
			})
	}, [productId])

	return (



	<Col className="mt-5 mb-5">
    <Form>

		<Form.Group controlId="formFile" className="mb-3">
			<Form.Label>Default file input example</Form.Label>
			<Form.Control 
				type="file"
				onChange={(e) => {setImage(e.target.value)}} 
			/>
		</Form.Group>

		<Form.Group className="mb-3" controlId="formBasicEmail">
			<Form.Label>Product Name:</Form.Label>
			<Form.Control 
				type="name" 
				value={name} 
				onChange={(e) => {setName(e.target.value)}}
				placeholder="Enter Product Name"
				required 
			/>
		</Form.Group>

		<Form.Group className="mb-3" controlId="formBasicEmail">
			<Form.Label>Product Price:</Form.Label>
			<Form.Control 
				type="price" 
				value={price} 
				onChange={(e) => {setPrice(e.target.value)}}
				placeholder="Enter Product Price"
				required 
			/>
		</Form.Group>

		<Form.Group className="mb-3" controlId="formBasicEmail">
			<Form.Label>Product Stocks:</Form.Label>
			<Form.Control 
				type="stocks" 
				value={stocks} 
				onChange={(e) => {setStocks(e.target.value)}}
				placeholder="Enter Product Stocks"
				required 
			/>
		</Form.Group>

		<Form.Group className="mb-3" controlId="formBasicEmail">
			<Form.Label>Product Description:</Form.Label>
			<Form.Control 
				as="textarea" 
				rows={3}
				type="description" 
				value={description} 
				onChange={(e) => {setDescription(e.target.value)}}
				placeholder="Enter Product Description" 
			required
		/>
		</Form.Group>
		<Button variant="secondary" as={NavLink} to={"/ManageProducts"}>
		Back
		</Button>
		<Button variant="success" onClick={() => updateProduct()}>
		Update Product
		</Button>

    </Form>
    </Col>
	)
}
