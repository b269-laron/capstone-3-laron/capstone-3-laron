import {useState, useEffect} from 'react';


// Components
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';
import ViewProductCard from './components/ViewProductCard';

// Pages
import Home from './pages/Home';
import Register from './pages/Register';
import Product from './pages/Product';
import Profile from './pages/Profile';
import Login from './pages/Login';
import Logout from './pages/Logout';



//Checking out related pages
//import Cart from './pages/Cart';

//Admin Pages
import AddProduct from './adminPages/AddProduct';
import ManageProducts from './adminPages/ManageProducts';
import ManageUser from './adminPages/ManageUser';

//Admin Components
import UpdateProduct from './adminComponents/UpdateProduct';
import ManageUserProfile from './adminComponents/ManageUserProfile';


import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css';

export default function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      //User is logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }


      //User is logged out
      else {
        setUser ({
          id: null,
          isAdmin: null
        })
      }

    })
  }, []);

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
        <Container>
          <Routes>
          {/*General Pages*/}
           < Route path="/" element={<Home/>}/> 
           < Route path="/register" element={<Register/>}/> 
           < Route path="/coffees" element={<Product/>}/> 
           < Route path="//users/:userId/profile" element={<Profile/>}/> 
           < Route path="/login" element={<Login/>}/> 
           < Route path="/logout" element={<Logout/>}/> 
           < Route path="/order" element={<Logout/>}/> 
           {/*< Route path="/checkout" element={<Checkout/>}/> */}
           
           {/*Checking out related pages*/}
            

           {/*Admin pages*/}
           < Route path="/addProduct" element={<AddProduct/>}/> 
           < Route path="/ManageProducts" element={<ManageProducts/>}/> 
           < Route path="/ManageUser" element={<ManageUser/>}/> 
           < Route path="/products/:productId" element={<UpdateProduct/>}/> 
           < Route path="/users/:userId" element={<ManageUserProfile/>}/> 
            
           < Route path="/products/:productId/checkout" element={<ViewProductCard/>}/> 
          </Routes>

        </Container> 
        <Footer/>
        </Router>
      </UserProvider>
    </>
  );
}

