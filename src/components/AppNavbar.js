import {Link, NavLink, useParams} from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';

import {Navbar, Nav, NavDropdown, Container} from 'react-bootstrap';

import { PersonCircle, CartFill  } from 'react-bootstrap-icons';



function BrandExample() {

   const {user} = useContext(UserContext);

  return (
      <Navbar sticky="top" bg="dark" variant="dark" expand="lg">
        <Container>
        <Navbar.Brand className="ms-4">KEOPI</Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto me-5">
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to="/coffees">Coffees</Nav.Link>

          

          { (user.id !== null) ?
          <>
          { (user.isAdmin === true) ?
            <>
            <NavDropdown title="Admin Dashboard"  id="collasible-nav-dropdown">
            <NavDropdown.Item as={NavLink} to="/ManageUser">Manage Users</NavDropdown.Item>
            
            <NavDropdown.Item as={NavLink} to={"/ManageProducts"}>Manage Products</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to={"/addProduct"}>Add Products</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
            </NavDropdown>
            </>
          :
            <>
            
            <NavDropdown title="Profile" id="collasible-nav-dropdown">
            <NavDropdown.Item as={Link} to={`/users/${user.id}/profile`}>Manage Profile</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
            </NavDropdown>
            <Nav.Link><CartFill size={24}/></Nav.Link>
            </>
          }
          </>
          :
          <>
          <Nav.Link as={NavLink} to="/login"><PersonCircle size={24}/></Nav.Link>
          <Nav.Link><CartFill size={24}/></Nav.Link>
          </>
          }

          </Nav>
        </Navbar.Collapse> 
        </Container>       
    </Navbar>


  );
}

export default BrandExample;