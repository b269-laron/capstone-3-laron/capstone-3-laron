import { useState, useEffect, useContext } from 'react';
import { Card, Button, Row, Col, Form } from 'react-bootstrap';

import { useParams } from 'react-router-dom'

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function CourseView() {

	const {user} = useContext(UserContext);

	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);


	
	const checkingOut = (productId, quantity) => {
		console.log(productId, quantity)
			fetch(`${process.env.REACT_APP_API_URL}/orders/createOrder`, {
				method: "POST",
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify ({
					products: 

					[{
						productId:productId,
						quantity: quantity
					}]
				})
			})

			.then(res => res.json())
			.then(data => {
				console.log(productId, quantity)

				if(data === true) {
					Swal.fire({
						title: "Order successful",
						icon: "success",
						text: ""
					})

				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}

			})
		};


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/productDetails`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
			})
	}, [productId])

	return (
	<Col className="mb-5" lg={{span: 8, offset: 2}}>	
		<Row>		
			<Col >
			<Card className="mt-5 p-4">
				<Card.Body className="text-center">
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PhP {price}</Card.Text>
				</Card.Body>
				<Form className="mb-3 mt-5">
				<Form.Label>Quantity:</Form.Label>
				<Form.Control 
				  type="Number" 
				  value={quantity} 
				  onChange={(e) => {setQuantity(e.target.value)}}
				  placeholder="Quantity"
				  required 
				 />
				</Form>

				{(user.isAdmin === true) ?
				<Button variant="success" disabled>Admin cannot place an order</Button>	
				:
				<Button variant="success" onClick={() => checkingOut(productId, quantity)}>Order</Button>
				}
				
			</Card>
			</Col>
		</Row>
	</Col>
	)
}
