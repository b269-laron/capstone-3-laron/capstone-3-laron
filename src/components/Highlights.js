import Carousel from 'react-bootstrap/Carousel';
import {Container, Row} from 'react-bootstrap';

import '../App.css';

function UncontrolledExample() {
  return (
    <div style={{ height: 1700}}>

    <div className="MyCarousel mb-5 container">
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../images/Artboard1.png')}
          alt="First slide"
        />
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../images/Artboard2.png')}
          alt="Second slide"
        />
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../images/Artboard3.png')}
          alt="Third slide"
        />
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../images/Artboard4.png')}
          alt="Fourth slide"
        />
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../images/Artboard5.png')}
          alt="Fifth slide"
        />
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../images/Artboard6.png')}
          alt="Sixth slide"
        />
      </Carousel.Item>
    </Carousel>
    </div>
    <div className="phrases">
        <Container>
          <Row>
            <div className="order-md-1 order-2 col-sm-12 col-md-6 pl-3 pb-3 align-self-center">
              <h5 className="font-weight-bold">Start your day off right with a delicious cup of coffee!</h5>
              <p className="text-justify ">
              If you're a coffee lover, there's nothing quite like the aroma of freshly brewed coffee in the morning. And what better way to enjoy that experience than by grinding your own coffee beans? Buying coffee beans allows you to enjoy the rich, full-bodied flavor of coffee in the comfort of your own home. Plus, you can customize your coffee to your own tastes by selecting the roast level, grind size, and origin of the beans. Whether you prefer a dark, bold roast or a light, fruity one, there's a coffee bean out there that's perfect for you. So why settle for pre-ground coffee when you can experience the ultimate coffee experience by grinding your own beans? Try it today and savor the rich, complex flavor of freshly brewed coffee.

              

              </p>
            </div>
            
            <div className="order-md-2 order-1 col-sm-12 col-md-6 pl-3 pb-3">
              <img 
              src={require('../images/blackcoffee.jpg')} 
              className="img-fluid"
              alt=""
              />
            </div>
          </Row>
        </Container>
      </div>

      <div>
       <Container className="col-sm-12 mt-5 ">
         <img 
         src={require('../images/Artboard7.png')} 
         className="img-fluid mb-4"
         alt=""
         />
         <p className="">
           Arabica coffee beans are a premium variety of coffee that offers a unique taste and aroma that is highly sought after by coffee enthusiasts around the world. When it comes to coffee quality, Arabica beans are the gold standard.
         </p>
         <h5>KEOPI</h5>
         <p className="mb-5">KEOPI is a newly established company that specializes in providing premium quality coffee beans to coffee lovers. Our focus is on delivering the best coffee experience to our customers through our carefully selected coffee beans. Our best seller is the Arabica bean, which we source from the finest coffee growing regions in the world. At KEOPI, we are passionate about coffee and are committed to bringing you the highest quality beans to make your daily cup of coffee an enjoyable and memorable experience.</p>
       </Container> 
      </div>

    </div>


    

  );
}

export default UncontrolledExample;