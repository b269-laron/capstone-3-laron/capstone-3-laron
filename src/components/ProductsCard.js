import {useContext} from 'react';
import UserContext from '../UserContext';


import {Button, Col, Card} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';

import {CartFill} from 'react-bootstrap-icons';


export default function ProductsCard({product}) {

    const {user} = useContext(UserContext);

    const { name, description, price, _id } = product;


return (

    <Col className="mb-5 mt-5" xs={12} md={6} lg={4}>
        <Card className="ProductsCard p-0">
            {/*<Card.Img variant="top" src={image} />*/}
            <Card.Body>
                <Card.Title className="mb-4"><h4>{name}</h4></Card.Title>
                <Card.Subtitle className="mb-2">Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
            </Card.Body>  
            <Card.Footer className="text-muted ">
            
            <Card.Text>Price: {price}</Card.Text>
            { (user.id !== null) ?
            <Button className="bg-primary col-12" as={Link} to={`/products/${_id}/checkout`}>
                <CartFill size={24}/> Buy now
            </Button>
            :
            <Button className="bg-secondary" as={NavLink} to={"/login"}>
                <CartFill size={24}/> Please login 
            </Button>
            }      
            </Card.Footer>  
        </Card>
    </Col>
    )
}