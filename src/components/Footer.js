import React from 'react';

function Footer() {
  return (
    <footer className="text-center text-lg-start bg-dark text-light">
      <section className="p-3">
        <div className="container text-center text-md-start mt-4">
          <div className="row mt-3">
            <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
              <h6 className="text-uppercase fw-bold mb-4">KEOPI</h6>
              <p>KEOPI is a newly established company that specializes in providing premium quality coffee beans to coffee lovers. Our focus is on delivering the best coffee experience to our customers through our carefully selected coffee beans.</p>
            </div>
            <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
              <h6 className="text-uppercase fw-bold mb-4">Products</h6>
              <p>
                <a href="#!" className="text-reset">Coffee Beans</a>
              </p>
              
            </div>

            <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

              <h6 className="text-uppercase mb-4">
                FAQ
              </h6>
              <p>
                <a href="#!" className="text-reset">About US</a>
              </p>
              <p>
                <a href="#!" className="text-reset">Orders</a>
              </p>
              <p>
                <a href="#!" className="text-reset">Help</a>
              </p>
            </div>

            <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

              <h6 className="text-uppercase mb-4">Contact</h6>
              <p><i className="me-3"></i> New York, NY 10012, US</p>
              <p>
                <i className="me-3"></i>
                info@example.com
              </p>
              <p><i className="me-3"></i> + 63 234 567 88</p>
              <p><i className="me-3"></i> + 63 234 567 77</p>
            </div>
          </div>
        </div>
      </section>



      <div className="text-center p-4" >
        © 2023 Copyright
      </div>

    </footer>
  );
}

export default Footer;
