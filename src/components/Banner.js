
import '../App.css';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function ImageWithText () {
  return (
    <Container className="phrases">
        <Row >
            <div >
                <h4 className="font-weight-bold">Start your day off right with a delicious cup of coffee!</h4>
                <p className="phrasestext text-justify ">If you're a coffee lover, there's nothing quite like the aroma of freshly brewed coffee in the morning. And what better way to enjoy that experience than by grinding your own coffee beans? Buying coffee beans allows you to enjoy the rich, full-bodied flavor of coffee in the comfort of your own home. Plus, you can customize your coffee to your own tastes by selecting the roast level, grind size, and origin of the beans. Whether you prefer a dark, bold roast or a light, fruity one, there's a coffee bean out there that's perfect for you. So why settle for pre-ground coffee when you can experience the ultimate coffee experience by grinding your own beans? Try it today and savor the rich, complex flavor of freshly brewed coffee.</p>
                </div>
                <div>
                    <img 
                    className="phrasesImg" 
                    src={require('../images/blackcoffee.jpg')}
                    class="img-fluid"
                    />
                </div>
        </Row>
    </Container>


       
  );
}

export default ImageWithText;