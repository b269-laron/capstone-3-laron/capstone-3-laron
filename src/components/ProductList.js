import {useState, useEffect, useContext} from 'react';
import { Row } from 'react-bootstrap'

import UserContext from '../UserContext';

import ProductsCard from '../components/ProductsCard';

export default function Coffee () {

    const {user} = useContext(UserContext);

    const [products, setProduct] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/all`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setProduct(data.map(product => {
                return(
                    <ProductsCard key={product._id} product={product} />
                )
            }))
        })
    }, [])

    return (
        <Row>
        {products}      
        </Row>
    )
}