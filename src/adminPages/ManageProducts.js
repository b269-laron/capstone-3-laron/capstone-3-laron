import {Row} from 'react-bootstrap';
import {useState, useEffect} from 'react'

import ManageProductsCard from '../adminComponents/ManageProductsCard'

export default function Cart() {

  const [allproducts, setAllProduct] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setAllProduct(data.map(product => {
        return(
          <ManageProductsCard key={product._id} product={product} />
        )
      }))
    })
  }, [])


  return (
      <Row className="justify-content-center col-lg-12 col-md-10 col-xs-12">
      {allproducts}
      </Row>
  );
} 
