import {Row} from 'react-bootstrap';
import {useState, useEffect} from 'react'

import ManageUserTable from '../adminComponents/ManageUserTable'

export default function Users() {

  const [allUsers, setallUsers] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/allUser`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setallUsers(data.map(user => {
        return(
          <ManageUserTable key={user._id} user={user} />
        )
      }))
    })
  }, [])


  return (
      <Row style={{ height: 480}}>
      <div className="mt-5 d-flex">
        <h4 className="col-3 ">Name</h4>
        <h4 className="col-3 ">Email</h4>
        <h4 className="col-3 ">Access</h4>
        <h4 className="col-3 ">Details</h4>
      </div>
      {allUsers}
      </Row>
  );
} 
